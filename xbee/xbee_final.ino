/*****************************************************************
XBee_Remote_Control.ino
Write your Arduino's pins (analog or digital) or read from those
pins (analog or digital) using a remote XBee.
Jim Lindblom @ SparkFun Electronics
Original Creation Date: May 7, 2014

This sketch requires an XBee, XBee Shield and another XBee tied to
your computer (via a USB Explorer). You can use XCTU's console, or
another serial terminal program (even the serial monitor!), to send
commands to the Arduino.

Example usage (send these commands from your computer terminal):
    s     - status
    p#v   - change ping period from old value to new value (v)
    t     - turn sensor on/off

    - Upper or lowercase works

Hardware Hookup:
  The Arduino shield makes all of the connections you'll need
  between Arduino and XBee. Make sure the SWITCH IS IN THE
  "DLINE" POSITION.

Development environment specifics:
    IDE: Arduino 1.0.5
    Hardware Platform: SparkFun RedBoard
    XBee Shield & XBee Series 1 1mW (w/ whip antenna)
        XBee USB Explorer connected to computer with another
          XBee Series 1 1mW connected to that.

This code is beerware; if you see me (or any other SparkFun
employee) at the local, and you've found our code helpful, please
buy us a round!

Distributed as-is; no warranty is given.

Debug tools:
#ifdef DEBUG
 #define DEBUG_PRINT(x)  Serial.println (x)
#else
 #define DEBUG_PRINT(x)
#endif

*****************************************************************/

// SoftwareSerial is used to communicate with the XBee
#include <SoftwareSerial.h>

#define DEBUG
#ifdef DEBUG
  #define DEBUG_PRINT(x)    Serial.print (x)
  #define DEBUG_PRINTLN(x)  Serial.println (x)
  #define DEBUG_READ()      Serial.read()
#else
#define DEBUG_PRINT(x)
#define DEBUG_PRINTLN(x)
#define DEBUG_READ()
#endif

#define SerialPort          XBee
//define SerialPort        Serial

int ledPin = 13;
int sensePin = 2;
bool sensor_active = true;
bool door_status = true;
volatile int value = 0;
int len = 0;

SoftwareSerial XBee(3, 4); // Arduino RX, TX (XBee Dout, Din)

// Install the interrupt service routine.
void ISRoutine() {
  // check the value again - since it takes some time to
  // activate the interrupt routine, we get a clear signal.
  value = digitalRead(sensePin);
  door_status = not value;
  door_change();
}

void setup()
{
  // Initialize XBee Software Serial port. Make sure the baud
  // rate matches your XBee setting (9600 is default).
  Serial.begin(9600);
  XBee.begin(9600);
  printMenu(); // Print a helpful menu:

  Serial.println("Initializing ihandler");
  // sets the digital pin as output
  pinMode(ledPin, OUTPUT);
  // read from the sense pin
  pinMode(sensePin, INPUT);
  Serial.println("Processing initialization");

  attachInterrupt(0, ISRoutine, CHANGE);
  Serial.println("Finished initialization");
}

void loop()
{
  digitalWrite(ledPin, value);
  //delay(ping_period); // ms
  //ping();
  check_input();
  //DEBUG_PRINTLN("HERE");
}

void check_input()
{
  // In loop() we continously check to see if a command has been
  //  received.
  if (SerialPort.available() > 0)
  {
    char c = SerialPort.read();
    //DEBUG_PRINTLN(c);
    switch (c)
    {
      case 't':      // If received 't'
      case 'T':      // or 'T'
        turn_on_off(); // Turn sensor on/off
        break;
      case 's':      // If received 's'
      case 'S':      // or 'S'
        print_status();  // Print status
        break;
      case 'p':      // If received 's'
      case 'P':      // or 'S'
        ping();  // Ping 
        break;
      default:
      report_error();
    }
  }
}

void print_status()
{
  XBee.print(F("t"));
  XBee.println(sensor_active);
  XBee.print(F(" d"));
  XBee.println(door_status);
  
  DEBUG_PRINTLN();
  DEBUG_PRINTLN(F("Arduino XBee Door Sensor!"));
  DEBUG_PRINTLN(F("============================"));
  DEBUG_PRINTLN(F("Status: "));
  DEBUG_PRINT(F("Sensor: "));
  DEBUG_PRINTLN((sensor_active) ? "Activated" : "Deactivated");
  DEBUG_PRINT(F("Door: "));
  DEBUG_PRINTLN((door_status) ? "Open" : "Closed");
  DEBUG_PRINTLN(F("============================"));
}

void door_change()
{
  digitalWrite(ledPin, door_status);
  if (sensor_active)
  {
    DEBUG_PRINT(F("Door: "));
    DEBUG_PRINTLN((door_status) ? "Opened " : "Closed ");
    XBee.print("d");
    XBee.println(door_status);
  }
}

void ping()
{
  DEBUG_PRINT("p");
  DEBUG_PRINTLN(door_status);
  XBee.print("d");
  XBee.println(door_status);
}

void turn_on_off()
{
  char arg[3] = { 0, 0, 0 };
  
  while (int len = SerialPort.available())
  {
      for (int i = 0; i < 3; i++)
      {
        arg[i] = SerialPort.read();
        
      }
  }

  if (((arg[0] == 'o') || (arg[0] == 'O')) &&
      ((arg[1] == 'n') || (arg[1] == 'N')))
  {
    sensor_active = true;
  }
  else if (((arg[0] == 'o') || (arg[0] == 'O')) &&
           ((arg[1] == 'f') || (arg[1] == 'F')) &&
           ((arg[2] == 'f') || (arg[2] == 'F')))
  {
    sensor_active = false;
  }
  else
  {
    report_error();
    return;
  }
    DEBUG_PRINT(F("t"));
    DEBUG_PRINTLN(sensor_active);
    XBee.print("t");
    XBee.println(sensor_active);
}

void report_error()
{
  DEBUG_PRINTLN("Wrong command");
  XBee.println("Wrong command");
}

// printMenu
// A big ol' string of Serial prints that print a usage menu over
// to the other XBee.
void printMenu()
{
  // Everything is "F()"'d -- which stores the strings in flash.
  // That'll free up SRAM for more importanat stuff.
  XBee.println();
  XBee.println(F("Arduino XBee Reed Switch Sensor!"));
  XBee.println(F("============================"));
  XBee.println(F("Usage: "));
  XBee.println(F("s   - display status"));
  XBee.println(F("p   - ping"));
  XBee.println(F("t   - turn door sensor on/off "));
  XBee.println(F("Upper or lowercase works"));
  XBee.println(F("============================"));
}