#!/usr/bin/env python
import serial

XBEE_DEVICE = "/dev/ttyUSB0"
XBEE_BAUD_RATE = 9600

def rdln(tty):
	line=""
	while True:
		char = tty.read()
		if char != '\r':
			line += char
		else:
			return line

def echo(tty, echo_string, skip_newline=False):
	tty.write(echo_string)
	if not skip_newline:
		tty.write("\r\n")

def cmd(tty, cmd_string, skip_newline=False):
	echo(tty, cmd_string, skip_newline)
	result = rdln(tty)
	if result == "OK":
		return True
	else:
		print("Error: %s" % result)
		return False

def init():
	print("Connecting to XBee at %s" % XBEE_DEVICE)

	tty = serial.Serial(XBEE_DEVICE, XBEE_BAUD_RATE)

	print("Connected")
	print("Testing the connection via +++")

	if cmd(tty, "+++", True):
		print("Success, exiting the command mode")
		if cmd(tty, "atcn"):
			return tty
	else:
		print("XBee initialiyation failed")
		return None


def main():
	print("Welcome to XBee test")
	tty = init()
	if not tty:
		return
	echo(tty, "Hello")
	while True:
		print(rdln(tty))

if __name__ == "__main__":
	main()
