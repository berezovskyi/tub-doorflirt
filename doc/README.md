## Introduction to FLIR Lepton

You can find detailed documentation under the `doc/flir` folder. To get yourself
accustomed with Raspberry and Lepton, run a sample program in `sample/` folder
and and you should see the `image.pgm` in the same folder that looks like this
(zoomed to 400%) (original is [image.pgm](image.pgm)):

![Sample image at 400%](sample.png)

More information can be found here:
https://github.com/PureEngineering/LeptonModule/wiki

In order to get the sensor working, you need to wire the things up properly:

**Breakout board**

![](pins_breakout.jpg)

**Pi's headers**

![](pins_headers.jpg)

## Raspberry Pi pinout

![](Raspberry-Pi-GPIO-pinouts.png)

And more extensively labeled version:

![](Raspberry-Pi-GPIO-Layout-Model-B-Plus-rotated-2700x900.png)
