USAGE INSTRUCTIONS
=========================

In order to execute commands on the node, supply the commands of the following
format to the UDP port 2000:

    show n (where n is 0..7), e.g.
    `show 7` (to turn all LEDs on) or
    `show 0` (to turn all LEDs off)
