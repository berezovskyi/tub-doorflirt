#include "printf.h"

module FlashC {
	uses interface Boot;
	uses interface HplMsp430GeneralIO as SwitchMsp;
	uses interface Timer<TMilli> as Timer;
	uses interface Leds;
}
implementation {

	enum {
		Read_PERIOD = 250, // ms
	};


	event void Boot.booted() {
		call SwitchMsp.selectIOFunc();
		call SwitchMsp.setResistor(0);
		call SwitchMsp.makeInput();
		call Timer.startPeriodic(Read_PERIOD);
	}

	event void Timer.fired() {
		if (call SwitchMsp.get()) {
			// +3V detected or reed switch closed
			call Leds.led0Off();
			call Leds.led2On();
		} else {
			call Leds.led2Off();
			call Leds.led0Toggle();
		}
	}
} // implementation
