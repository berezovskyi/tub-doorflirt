configuration ConsoleToLedAppC {}
implementation {
	components MainC;
	components FlashC;
	components LedsC;
	components SerialPrintfC;
	components HplMsp430GeneralIOC as GeneralIOC;

	components new TimerMilliC() as Timer;

	FlashC -> MainC.Boot;
	FlashC.Timer -> Timer;
	FlashC.SwitchMsp -> GeneralIOC.Port60;
	FlashC.Leds -> LedsC;
}
