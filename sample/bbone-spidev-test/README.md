This is an SPI test program from the linux kernel repository. Source is an old
version that doesn't include Feb'14 changes to support Dual/Quad SPI Transfers.

> http://www.raspberrypi.org/forums/viewtopic.php?f=33&t=73873
>
> From a little digging it appears that spidev_test.c has recently (February 
> 2014) been updated to Add support for Dual/Quad SPI Transfers. You need to get
> an older version of spidev_test.c that matches our older kernel.
>
> So you are doing the right thing (no make file required), but you just need to
> get an older version of spidev_test.c, such as this.

	$ make clean && make
	$ ./spidev_test

	Expected output:

	spi mode: 0
	bits per word: 8
	max speed: 500000 Hz (500 KHz)

	FF FF FF FF FF FF 
	FF FF FF FF FF FF 
	FF FF FF FF FF FF 
	FF FF FF FF FF FF 
	FF FF FF FF FF FF 
	FF FF FF FF FF FF 
	FF FF 

