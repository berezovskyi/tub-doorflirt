## Running from the command-line

	make clean && make
	./raspberry_pi_capture

## Viewing the output from the command line

Before using the script, install necessary packages:

	sudo apt-get install imagemagick jp2a

After that, run

	./show_ascii

You should see the ASCII-art representation of the image similar to this:

	root@beaglebone:~/tub-doorflirt/sample# sleep 2 && ./raspberry_pi_capture && ./show_ascii 
	spi mode: 0
	bits per word: 8
	max speed: 16000000 Hz (16000 KHz)
	Calculating min/max values for proper scaling...
	maxval = 8314
	minval = 7853
	...........                                                                ..,;c
	..............                                                         ..',codk0
	...............                                                   ....,;cox0K0Ox
	................                                                ...,;cldO00Od:'.
	...................                          ..               ..,;coxOOOxl;'.   
	....................... ..          ........'''''....      ..,;coxkkkdc,..      
	...............................   .....;:coddddddolc;'....,;codxxxl;'..         
	....................................':lxkOOOOOOOOkkxxdl::cldddoc,..             
	...................................;okO00000KKKKK00000Oxddol;'..                
	..................................;oO00000KKKKKKKKKKKKKOxc'..                   
	.................................'cxO00KKKKKKKKKKKKKKKK0kl..                    
	.................................,okO000000KKKKKKKKKKK00kl'.                    
	.................................;ok0000000KKKKKKKK0000Oxc...                   
	................................,lxO0KKKKKKKKK0OkOKKK00Ox;.....                 
	................,;'.............:dkOO00KKKKK0kxooxOKK00Od,......                
	...............,cc;.............:dkOO0000KK0Odlclok0000Oo'......                
	...............:ol;.............,lxOOO00000KOxddddk0000kc........               
	..............,lol;......,;:,....,lkOOO000KKK000000KKK0x;........  .            
	.............'cddl,....':ll:'.....;xOO00KKKXKKKKKKKKK0Oo'......                 
	.............;oxdc'...;loo:'......,dO00KKKXXXXXXKKXXK0x:......                  
	............'cxxo;..,cddoc'.......,dO00KKKXXXXXXXXKK0Ol'.......                 
	........,:llldkxl,':oxdo:'........,d00KKKKKKKKKKKKKKOo,..........               
	.......':odxxkOkxodxxxo;........':lkKKKKKKKKKKKKKKK0d;...........               
	.....,:cooddxOOOOOkkxo;........:dkO0KXXXKKKKKKKKKKK0d;............              
	...':odddddxkkOOOOOko;.....'',;oOKXXXXXXXXXKKKKKKKKOo:,,'''.........            
	..'cdxxddddxxkkOOOOkoc::::::::coOXNNXXXXXXXKKKKKKK0dc:;::cc:;'..........        
	..;dkOxddxkkxxxkkOOkkxdddxollllld0NNXXXXXXKKKKKXK0xc:c::;:clllcclol:;,'..       
	.,lkOOkxxk00OkkxkkkkxddxkkxdolllodOKXXXXXKKKKKK0kocclodollllloldkkxdddlc;'.     
	':dOOOOO0000OOkkkkkxdddxkkkdolllloodxO0KXKK0Oxdllclloddddxxxolodkkxdddoool:'.   
	':dOOO00KK0OOOOOkkkxddxkkkkxdollllloodxkO0Oxoollllloodddxxddoloxkxddddddddoc,. 
