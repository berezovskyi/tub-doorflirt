#include "gtest/gtest.h"
#include <curl/curl.h>

TEST(GoogleTest, IsInitialized) {
	SUCCEED();
}

TEST(CURL, HasAccessToInternet) {
	curl_global_init(CURL_GLOBAL_ALL);
	CURL *curl_handle;
	CURLcode result;
	curl_handle = curl_easy_init();

	curl_easy_setopt(curl_handle, CURLOPT_URL, "http://google.com");
	curl_easy_setopt(curl_handle, CURLOPT_NOBODY, 1);

	result = curl_easy_perform(curl_handle);
	curl_easy_cleanup(curl_handle);
	if (result == CURLE_OK) {
		SUCCEED();
	} else {
		FAIL() << "CURL failed to fetch Google homepage: " << curl_easy_strerror(result);
	}
}