#pragma once

//#define DEBUG

#define ARRAY_SIZE(a) (sizeof(a) / sizeof((a)[0]))
#define VOSPI_FRAME_SIZE (164)

void lepton_init();
unsigned int **lepton_capture();
void lepton_save(const char*);
void lepton_close();
