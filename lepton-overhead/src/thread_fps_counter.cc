#include "globals.h"

void func_fps_counter(int &counter) {
    using namespace std::chrono;
    while (true) {
        seconds t(1);
        this_thread::sleep_for(t);
        if (counter == 0) {
            return;
        }
        log("FPS: " << counter);
        counter = 0;
    }
}