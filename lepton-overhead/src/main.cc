#include "globals.h"
#include "curl.h"
#include <unistd.h>

volatile bool tok_active = false;
volatile bool tok_door_open = false;
volatile unsigned int tok_period = -1;
volatile unsigned int tok_upload_interval = -1;

const char* test_capture();

typedef struct t_capture {
    volatile bool capture_enabled;
    volatile int duration;
} t_capture;


typedef struct t_control {
    volatile bool active;
    volatile unsigned int period;
} t_control;

volatile t_capture cfg;
volatile t_control config;

#define ms 1000

int door_state;

bool is_capture_enabled() {
    return config.active && (door_state == 1);
}

void func_capture(volatile t_capture& cfg) {
    lepton_init();  
    while (true) {
        if(!is_capture_enabled()) {
            usleep(1000 * ms);
        } else {
            const char* filename = test_capture();
            printf("%s\n", filename);
            curl_upload(filename);
            usleep(cfg.duration * ms);
        }
    }
}

serial::Serial* tty;

serial::Serial* init_tty()
{
    string port("/dev/ttyUSB0");
    unsigned long baud = 9600;

    serial::Serial* my_serial = new serial::Serial(port, 
                        baud, 
                        serial::Timeout::simpleTimeout(1000));

    cout << "Is the serial port open?";
    if(my_serial->isOpen())
        cout << " Yes." << endl;
    else
        cout << " No." << endl;

    return my_serial;
}

void serial_write_line(string test_string, serial::Serial& tty){
    int count = 0;
    tty.setTimeout(serial::Timeout::max(), 250, 0, 250, 0);
    while (count < 1) {
         tty.write(test_string);
        count += 1;
    }
}

string serial_read_line (serial::Serial& my_serial){
    string result = "";
    std::string eol = "\n";
    
    // Set the timeout at 250ms
    my_serial.setTimeout(serial::Timeout::max(), 250, 0, 250, 0);

    result = my_serial.readline(65536,eol);
    return result;
}

void serial_send_enable(bool tok_active, serial::Serial& tty) {
    serial_write_line(tok_active ? "ton" : "toff", tty);
    string response = serial_read_line(tty);
    if (strncmp(tok_active ? "t1" : "t0", response.c_str(), 2) != 0) {
        log("error enabling the serial: " << response);
    }
}

int serial_send_ping(serial::Serial& tty) {
    serial_write_line("p", tty);
    string response = serial_read_line(tty);
    if (strncmp("d0", response.c_str(), 2) == 0) {
        return 0;
    } else if (strncmp("d1", response.c_str(), 2) == 0) {
        return 1;
    } else {
        return -1;
    }
}

int main() {
    cfg.capture_enabled = false;
    cfg.duration = 100;
    thread capture_thread{func_capture, ref(cfg)};

    tty = init_tty();

    while(true) {

        /* Read JSON config*/
        bool bak_active = tok_active;
        unsigned int bak_period = tok_period;
        unsigned int bak_upload_interval = tok_upload_interval;

        curl_config();

        /*change?*/
        config.active = tok_active;
        if (tok_active != bak_active) {
            serial_send_enable(tok_active, *tty);
        }

        cfg.duration = tok_upload_interval;
        // if (tok_upload_interval != bak_upload_interval) {
        // }

        config.period = tok_period;
        // if (tok_period != bak_period) {
        // }

        if(tty->available() > 0) {
            string line = serial_read_line(*tty);
            const char* line_c = line.c_str();
            door_state = -1;
            if(line_c[0] == 'd') {
                if(line_c[1] == '0') {
                    door_state = 0;
                } else if (line_c[1] == '1') {
                    door_state = 1;                    
                }
            }

            log("door_state " << door_state);

            // if(door_state != -1) {
            //     // cfg.capture_enabled = config.active && (door_state == 1);
            //     printf("line: %d\n", door_state);
            // }
        }
            
        // printf("capture_enabled = %s\n", cfg.capture_enabled ? "true" : "false");

        usleep(config.period * ms);
    }

    capture_thread.join();
	return 0;
}