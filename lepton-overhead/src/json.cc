#include "globals.h"
#include "json.h"

int tok_active_id = -1;
int tok_door_open_id = -1;
int tok_period_id = -1;
int tok_upload_interval_id = -1;

void dbg_tok(jsmntok_t& token, mem_struct& chunk) {
    int token_len = token.end - token.start;
#ifdef DEBUG
    printf("%.*s\n", token_len, chunk.memory + token.start);
#endif
}

bool parse_bool(jsmntok_t token, mem_struct& chunk) {
    int token_len = token.end - token.start;

    if(strncmp("true", chunk.memory + token.start, sizeof("true")-1) == 0) {
        return true;
    }
    return false;
}

unsigned int parse_int(jsmntok_t token, mem_struct& chunk) {
    int token_len = token.end - token.start;
    string token_string = string(chunk.memory + token.start, token_len);

    return strtoul(token_string.c_str(), NULL, 10);
}

void try_assign_token(int i, jsmntok_t token, mem_struct& chunk) {
    d("assigning token... " << i);
    if(i == tok_active_id) {
        tok_active = parse_bool(token, chunk);
        log("tok_active: " << tok_active);
    } else if(i == tok_door_open_id) {
        tok_door_open = parse_bool(token, chunk);
        log("tok_door_open: " << tok_door_open);
    } else if(i == tok_period_id) {
        tok_period = parse_int(token, chunk);
        log("tok_period: " << tok_period);
    } else if(i == tok_upload_interval_id) {
        tok_upload_interval = parse_int(token, chunk);
        log("tok_upload_interval: " << tok_upload_interval);
    } else {
        // d("..none assigned");
    }
}


void json_process(mem_struct& chunk) {
    jsmn_parser parser;
    jsmntok_t tokens[TOK_BUFFER];

    jsmn_init(&parser);

    // js - pointer to JSON string
    // tokens - an array of tokens available
    // TOK_BUFFER - number of tokens available
    int tokens_read = jsmn_parse(&parser, chunk.memory, chunk.size, tokens, TOK_BUFFER);

    for (int i = 0; i < tokens_read; ++i)
    {
        jsmntok_t tok = tokens[i];
        int token_len = tok.end - tok.start;
#ifdef DEBUG
        printf("tok %d: %d %d\n", i, (unsigned int)tok.type, token_len);
#endif
        try_assign_token(i, tok, chunk);
        if(tok.type == JSMN_STRING) {
#ifdef DEBUG
            printf(">>>%.*s\n", token_len, chunk.memory + tok.start);
#endif
            if(strncmp("active", chunk.memory + tok.start, sizeof("active")-1) == 0) {
                tok_active_id = i+1;
                continue;
            } else if(strncmp("door_open", chunk.memory + tok.start, sizeof("door_open")-1) == 0) {
                tok_door_open_id = i+1;
                continue;
            } else if(strncmp("period", chunk.memory + tok.start, sizeof("period")-1) == 0) {
                // log("tok_period_id = " << i+1);
                tok_period_id = i+1;
                continue;
            } else if(strncmp("upload_interval", chunk.memory + tok.start, sizeof("upload_interval")-1) == 0) {
                tok_upload_interval_id = i+1;
                continue;
            }
        }
    }

    // printf("active: %s\n", tok_active ? "true" : "false");
    // printf("door_open: %s\n", tok_door_open ? "true" : "false");
    // printf("period: %d\n", tok_period);
    // printf("upload_interval: %d\n", tok_upload_interval);
}