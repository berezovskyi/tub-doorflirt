#ifndef _GLOBALS_H
#define _GLOBALS_H

#include <iostream>
#include <cstdio>
#include <string>
#include <thread>
#include <chrono>
#include <functional>

using namespace std;

#include <opencv2/opencv.hpp>
#include <cvblob.h>

#include "lepton.h"
#include "serial/serial.h"
#include "jsmn.h"
#include "curl/curl.h"

// #define DEBUG
// #define CURL_DEBUG
#define TOK_BUFFER 64
#ifdef DEBUG
#endif

#define log(str) {cout << str << endl;}
#ifdef DEBUG
#define d(str) {cout << str << endl;}
#else
#define d(str) {}
#endif

#endif