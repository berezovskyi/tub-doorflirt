#include "globals.h"
#include "curl.h"
#include "json.h"
 

void curl_upload(const char* filename) {
    char curlCommand [512];
    sprintf(curlCommand, "curl --form \"stuff_image=@%s\" --form title=lepton https://tub-doorflirt.herokuapp.com/stuff/create/", filename);
    system(curlCommand);
    log("curl OK");
}

size_t curl_callback(void *contents, size_t size, size_t nmemb, void *userp)
{
#ifdef DEBUG
 printf("New chunk: %dB\n", size);
#endif
    size_t realsize = size * nmemb;
    struct mem_struct *mem = (struct mem_struct *)userp;
 
    mem->memory = (char*)realloc(mem->memory, mem->size + realsize + 1);
    if(mem->memory == NULL) {
        /* out of memory! */ 
        printf("not enough memory (realloc returned NULL)\n");
        return 0;
    }
 
    memcpy(&(mem->memory[mem->size]), contents, realsize);
    mem->size += realsize;
    mem->memory[mem->size] = 0;
 
    return realsize;
}

void curl_config() {
    CURL *curl;
    CURLcode res;
    struct mem_struct chunk;
 
    chunk.memory = (char*)malloc(1);  /* will be grown as needed by the realloc above */ 
    chunk.size = 0;    /* no data at this point */ 

    curl_global_init(CURL_GLOBAL_DEFAULT);
 
    curl = curl_easy_init();
    if(curl) {
        curl_easy_setopt(curl, CURLOPT_URL, "https://tub-doorflirt.herokuapp.com/stuff/config/");
        curl_easy_setopt(curl, CURLOPT_HTTPGET,1); 

#ifdef CURL_DEBUG
        curl_easy_setopt(curl, CURLOPT_HEADER,1); 
#endif

        /* send all data to this function  */ 
        curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, curl_callback);
     
        /* we pass our 'chunk' struct to the callback function */ 
        curl_easy_setopt(curl, CURLOPT_WRITEDATA, (void *)&chunk);


        /* Perform the request, res will get the return code */ 
        res = curl_easy_perform(curl);
        /* Check for errors */ 
        if(res != CURLE_OK) {
            fprintf(stderr, "curl_easy_perform() failed: %s\n",
                            curl_easy_strerror(res));
        } else {
#ifdef DEBUG
                printf("Response is %d bytes long\n", (int)chunk.size);
                printf("%s\n", chunk.memory);
#endif
                json_process(chunk);
        }

        if(chunk.memory)
            free(chunk.memory);


        /* always cleanup */ 
        curl_easy_cleanup(curl);
    }
 
    curl_global_cleanup();
}