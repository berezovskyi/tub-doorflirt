#pragma once

#include "curl.h"

extern bool tok_active;
extern bool tok_door_open;
extern unsigned int tok_period;
extern unsigned int tok_upload_interval;

void json_process(mem_struct& chunk);