/***
 * This example expects the serial port has a loopback on it.
 *
 * Alternatively, you could use an Arduino:
 *
 * <pre>
 *  void setup() {
 *    Serial.begin(<insert your baudrate here>);
 *  }
 *
 *  void loop() {
 *    if (Serial.available()) {
 *      Serial.write(Serial.read());
 *    }
 *  }
 * </pre>
 */

#include <string>
#include <iostream>
#include <cstdio>

// OS Specific sleep
#ifdef _WIN32
#include <windows.h>
#else
#include <unistd.h>
#endif

#include "serial/serial.h"

using std::string;
using std::exception;
using std::cin;
using std::cout;
using std::cerr;
using std::endl;
using std::vector;
using std::getline;
using namespace serial;

void my_sleep(unsigned long milliseconds) {
#ifdef _WIN32
			Sleep(milliseconds); // 100 ms
#else
			usleep(milliseconds*1000); // 100 ms
#endif
}

void enumerate_ports()
{
	vector<PortInfo> devices_found = list_ports();

	vector<PortInfo>::iterator iter = devices_found.begin();

	while( iter != devices_found.end() )
	{
		PortInfo device = *iter++;

		printf( "(%s, %s, %s)\n", device.port.c_str(), device.description.c_str(),
		 device.hardware_id.c_str() );
	}
}

void print_usage()
{
	cerr << "Usage: test_serial {-e|<serial port address>} ";
		cerr << "<baudrate> [test string]" << endl;
}

serial::Serial* init(int argc, char **argv)
{
	if(argc < 2) {
		print_usage();
		return NULL;
	}

	// Argument 1 is the serial port or enumerate flag
	string port(argv[1]);

	if( port == "-e" ) {
		enumerate_ports();
		return NULL;
	}
	else if( argc < 3 ) {
		print_usage();
		return NULL;
	}

	// Argument 2 is the baudrate
	unsigned long baud = 0;
#if defined(WIN32) && !defined(__MINGW32__)
	sscanf_s(argv[2], "%lu", &baud);
#else
	sscanf(argv[2], "%lu", &baud);
#endif

	// port, baudrate, timeout in milliseconds
	serial::Serial* my_serial = new serial::Serial(port, baud, serial::Timeout::simpleTimeout(1000));

	cout << "Is the serial port open?";
	if(my_serial->isOpen())
		cout << " Yes." << endl;
	else
		cout << " No." << endl;

	return my_serial;
}

string read_keyboard (){
	// Get string
	string mystr = "";
  	cout << "Enter command: ";
 	getline (cin, mystr);
  return mystr;
}

void serial_write_line(string test_string, serial::Serial& tty){
	int count = 0;
	tty.setTimeout(serial::Timeout::max(), 250, 0, 250, 0);
	while (count < 1) {
		 tty.write(test_string);
		count += 1;
	}
}

string serial_read_line (serial::Serial& my_serial){

	string result = "";
	std::string eol = "\n";
	
	// Set the timeout at 250ms
	my_serial.setTimeout(serial::Timeout::max(), 250, 0, 250, 0);

	result = my_serial.readline(65536,eol);
	return result;
}


void serial_print_lines (serial::Serial& my_serial, int lines){
	string result = "";
	int line_number = 0;
	while (line_number < lines) {
		result = serial_read_line(my_serial);
		cerr <<"Response: " << result << endl;
		line_number += 1;
	}
}

int main(int argc, char **argv) {

	string result = "";
	string ping = "p";
	string status = "s";
	string turn_on = "ton";
	string turn_off = "toff";

	string test_string = "";
	try {
		serial::Serial* tty = init(argc, argv);
		while (true) {
			serial_write_line(ping, *tty);
			cout << "Command: " << ping << endl;
			serial_print_lines(*tty, 1);
				
			serial_write_line(status, *tty);
			cout << "Command: " << status << endl;
			serial_print_lines(*tty, 1);
			
			serial_write_line(turn_on, *tty);
			cout << "Command: " << turn_on << endl;
			serial_print_lines(*tty, 1);
			
			serial_write_line(turn_off, *tty);
			cout << "Command: " << turn_off << endl;
			serial_print_lines(*tty, 1);

/*			if (tty.available())
			{
				serial_print_lines(*tty, 1);
			}
			else 
			{

				serial_write_line(ping, *tty);
				serial_print_lines(*tty, 1);
				
				serial_write_line(status, *tty);
				serial_print_lines(*tty, 1);
				
				serial_write_line(turn_on, *tty);
				serial_print_lines(*tty, 1);
				
				serial_write_line(turn_off, *tty);
				serial_print_lines(*tty, 1);

				result = read_keyboard();
			}*/
		}
	} catch (exception &e) {
		cerr << "Unhandled Exception: " << e.what() << endl;
	}
}
