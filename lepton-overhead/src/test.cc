#include "globals.h"

void test_mat() {
    cv::Mat M = cv::imread("image.pgm", CV_LOAD_IMAGE_GRAYSCALE);
    for (int i = 0; i < M.rows; i++) {
        for (int j = 0; j < M.cols; j++)
            cout << (uint)M.at<uchar>(i,j) << "  ";
        cout << endl;
    }
}

void test_serial() {
    serial::Serial my_serial("/dev/ttyUSB0", 9600, serial::Timeout::simpleTimeout(1000));
}

const char* test_capture() {
    lepton_capture();
    log("capture OK");
    char filenamePgm [L_tmpnam];
    char* filenamePng = (char*)malloc(L_tmpnam+4);
    const char* ptrFilenamePgm = tmpnam(filenamePgm);
    const char* ptrFilenamePng = tmpnam(filenamePng);
    lepton_save(ptrFilenamePgm);
    log("save OK");
    char systemCommand [L_tmpnam+255];
    sprintf(filenamePng, "%s.jpg", ptrFilenamePng);
    sprintf(systemCommand, "convert %s -depth 8 -quality 75 -resize 400%% %s", ptrFilenamePgm, ptrFilenamePng);
    system(systemCommand);
    log("png convert OK");

    return filenamePng;
}
