## What is this?

It's a main project in our final Ad-hoc assignment. It consists of 3 parts:

1. Door switch communication code (via serial communication with XBee)
2. People counting code (getting images via SPI interface and using OpenCV
	for counting)
3. Network communication code (sending the information to Heroku and updating
	settings of the program itself as well as the door switch controller)

## How to build?

First, install the required dependencies:

	sudo apt-get install cmake libcurl4-openssl-dev libopencv-dev gcc-4.7 g++-4.7

In this folder, run the following commands (or use `-4.8` or any newer version if you have it):

	git pull
	cd build/
	CC=gcc-4.7 CXX=g++-4.7 cmake ..
	make
	ctest

To run the program, execute the following while in the `build` directory:

	./bin/lepton-overhead

## What if everything fails?

Navigate to the `build` directory and execute the following:

	cd .. && rm -rf build/ && md build && cd build/
	cmake .. && make && ctest --verbose

If it still fails, copy all the output and paste it to the new issue for me.

If the application itself fails, send me the core dump:

	ulimit -c unlimited
	./bin/lepton-overhead
