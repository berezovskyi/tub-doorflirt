> More information can be found here: http://elinux.org/Beagleboard:Cape_Expansion_Headers#2_SPI_Ports

## Device tree overlay

Before working with SPI on BeagleBone, you must enable them through the DTO.

There are some environment variables that must be set up beforehand, see the
`dtoenv` file. In order to load it automatically, append the following code to
the `.bashrc`:

```
if [ -e ~/tub-doorflirt/bbone/dtoenv ]; then
    . ~/tub-doorflirt/bbone/dtoenv
fi
```

This assumes you've cloned this repository to the home folder.

## Overlay commands

* `cat $SLOTS` shows loaded overlays. **L** indicates an active overlay.
* `echo _OVERLAY_ > $SLOTS` loads some `_OVERLAY_` into DTO.

## SPI pinout

![SPI pinout](spi_ports.png)

*From http://elinux.org/Beagleboard:Cape_Expansion_Headers#2_SPI_Ports*

